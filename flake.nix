{
  description = "Custom Nix Utilities";

  outputs = { self }: rec {
    # Apply two functions in series.
    lib.pipe = f1: f2: v: f2 (f1 v);
    # Thread a value through a list of functions in series.
    lib.thread = builtins.foldl' lib.pipe (v: v);
  };
}
